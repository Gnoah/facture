import { Client } from "./client";

export class Facture{
    id: number;
    client: Client;
    montant: number;
    date_creation: Date;

    constructor(){
        this.client = new Client();
    }
}