import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'dateMoment' })
export class Datepipe implements PipeTransform {

  transform(input: string): string {
    return moment(input).format('DD/MM/YYYY , h:mm a');
  }
}