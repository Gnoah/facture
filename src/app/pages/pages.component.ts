import { Component, OnInit, ViewChild } from '@angular/core';
import { Facture } from '../@core/entity/facture';
import { MatDialog } from '@angular/material/dialog';
import { DialogModalComponent } from './dialog-modal/dialog-modal.component';
import * as moment from 'moment';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import * as _ from 'lodash';
import { Client } from '../@core/entity/client';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  displayedColumns: string[] = ['nom', 'prenom', 'numero', 'montant', 'date_create'];
  listeFacture: [] = this.dataBuilder(table);

  dataTable = new MatTableDataSource(this.listeFacture);
  @ViewChild(MatSort) sort: MatSort;

  facture: Facture = new Facture();
  recherche: string;
  numFacture: number;
  events: string[];
  tab: Array<Facture> = new Array<Facture>();
  isLoading: boolean = false;
  isNumFacture: boolean = false;
  date1: Date;
  date2: Date;

  constructor(
    public dialog: MatDialog
  ) {
    moment.locale('fr-ca');
  }
  ngAfterViewInit() {
    this.dataTable.sort = this.sort;
  }

  ngOnInit(): void {
    this.dataTable.sort = this.sort;
  }

  dataBuilder(allData: Array<Facture>) {
    let dataAll: any = [];
    _.forEach(allData, function (data: Facture) {
      let currentData = { id: data.id, nom: data.client.nom, prenom: data.client.prenom, numero: data.client.numero, montant: data.montant, date_create: data.date_creation}
      dataAll = _.concat(dataAll, [currentData])
    })
    return dataAll;
  }
  sortChange() {
    this.dataTable.sort = this.sort;
  }

  format(date: Date): string {
    return moment(date).format('DD/MM/YYYY');
  }

  addDate(event: MatDatepickerInputEvent<Date>) {
    //this.date1 = moment(event.value).format('DD/MM/YYYY');
    this.date1 = moment(event.value).toDate();
    this.listeFacture = this.dataBuilder(this.filterByDate([this.date1, this.date2]));
    this.dataTable;
    this.dataTable = new MatTableDataSource(this.listeFacture);
  }
  addDate2(event: MatDatepickerInputEvent<Date>) {
    this.date2 = moment(event.value).toDate();
    this.listeFacture = this.dataBuilder(this.filterByDate([this.date1, this.date2]));
    this.dataTable;
    this.dataTable = new MatTableDataSource(this.listeFacture);   
  }

  filterByDate(filter: Array<Date>) {
    let resultFiler: Facture[];
    if (filter[0] && filter[1]) {
      resultFiler = table.filter(function (element) {
        return element.date_creation >= filter[0] && element.date_creation <= filter[1]
      })
    } else {
      resultFiler= table ;
    }
    return resultFiler;
  }

  onFind() {
    this.search(["nom", "prenom", "numero"], this.recherche, [this.date1, this.date2]);
  }

  onSubmit(){
    this.facture.date_creation = new Date();
    this.facture.id = table[table.length - 1].id + 1;
    table.push(this.facture);
    this.listeFacture = this.dataBuilder(table);
    this.dataTable;
    this.dataTable = new MatTableDataSource(this.listeFacture);
  }

  openDialog(): void {
    if(this.numFacture){
      this.isNumFacture = false;
      const dialogRef = this.dialog.open(DialogModalComponent, {
        width: '300px',
        data: { numero: this.numFacture, nom: "", prenom: "" } as Client,
      });
      dialogRef.afterClosed().subscribe(result => {
        if(result){
          this.facture.client.numero = result; 
          this.delete(result);
        }else{
          console.log("No");
        }
      });
    }else{
      this.isNumFacture = true;
    }
  }

  delete( numero:Number){
    table = table.filter(function(facture:Facture){
      return facture.client.numero != numero
    })
    this.listeFacture = this.dataBuilder(table);
    this.dataTable;
    this.dataTable = new MatTableDataSource(this.listeFacture);   
  }​


  search(keys: Array<string>, value: string, filter: Array<Date>) {
    if (value) {
      table = _.filter(table, function (o: Facture) {
        return resolveConditionSearch(keys, value, o);
      }.bind(this));
      console.log(table);
      this.listeFacture = this.dataBuilder(table);
      this.dataTable;
      this.dataTable = new MatTableDataSource(this.listeFacture);
    } else {
      console.log(table);
    }
  }
}

function resolveConditionSearch(keys: Array<string>, value: string, element: any): boolean {
  let condition: any = null;
  _.map(keys, key => {
    let property :string = <string>element.client[key];
    condition = condition || property == value[0].toUpperCase()+value.slice(1);
  })
  return condition;
}​

let table: Array<Facture> = [
  {
    "id": 2,
    "client": {
      "nom": "Musk",
      "prenom": "Alon",
      "numero": 11
    },
    "montant": 20000,
    "date_creation": new Date(2022,1,19)
  },
  {
    "id": 1,
    "client": {
      "nom": "Jane",
      "prenom": "Marie",
      "numero": 12
    },
    "montant": 4000,
    "date_creation": new Date(2022,2,13)
  },
  {
    "id": 3,
    "client": {
      "nom": "Rakoto",
      "prenom": "Zafy",
      "numero": 13
    },
    "montant": 5000,
    "date_creation":new Date(2021,12,19)
  },
  {
    "id": 4,
    "client": {
      "nom": "Rabe",
      "prenom": "Test",
      "numero": 16
    },
    "montant": 4500,
    "date_creation": new Date(2022,3,1)
  },
  {
    "id": 6,
    "client": {
      "nom": "Marie",
      "prenom": "Yan",
      "numero": 18
    },
    "montant": 4800,
    "date_creation": new Date(2021,11,11)
  }
]